import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import Plugins from './Plugins/Plugins'
import vueWechatTitle from 'vue-wechat-title'
import {Button,Form,Field,Icon,CountDown,Toast,Tabbar,TabbarItem,Lazyload,Swipe,SwipeItem} from 'vant';
import axios from 'axios'
import VueAxios from 'vue-axios'
import store from './store/index'
import '../public/lib/iconfont'

Vue.prototype.$apiURL=" http://localhost:3000"

Vue.config.productionTip = false

Vue.use(Plugins)
Vue.use(vueWechatTitle);
Vue.use(Button);
Vue.use(Form);
Vue.use(Field);
Vue.use(Icon);
Vue.use(CountDown);
Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(Toast);
Vue.use(VueAxios,axios);
Vue.use(Lazyload)
Vue.use(Swipe)
Vue.use(SwipeItem)

//全局过滤器
Vue.filter( 'listArr',function(value){
  let arrStr= ``
  if(value)
  {
    value.forEach(item=>{
      arrStr += item.name+" "
  })
  }
  return arrStr
}),


axios.interceptors.request.use( config=>{
  const cacheData = JSON.parse( localStorage.getItem(config.url) || null)
  if(cacheData)
  {
    if( Date.now() - cacheData.time<100000)
    {
      return Promise.reject({
        message:"reject",
        url:config.url
      })
    }
  }
  return config
},)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
