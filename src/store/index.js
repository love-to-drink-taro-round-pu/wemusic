import Vue from 'vue'
import VueX from 'vuex'
import state from './state'
import mutations from './mutations'

Vue.use(VueX)

const store = new VueX.Store({
    state,
    mutations
})

export default store