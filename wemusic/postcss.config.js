module.exports = {
    plugins: {
      'postcss-px-to-viewport': {
        viewportWidth: 320,
        exclude:/node_modules/i,
        selectorBlackList: ['play']
      },
    },
  };